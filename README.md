### Hi! Welcome to the repository hosting the Disaster Monitoring and Decision Support project.

#### This project started in the 2018 GOSH edition in Shenzhen and the idea is to *still need to add a brief description of the project here.*



You can contribute by sending/sharing information about projects that are/can be used in Disaster situations (see below):

To create the database on Disaster Monitoring and Decision Support the following details are requested

Name of the product / design

http:// link to the original website source/ picture / video

Brief description: Usefulness and relevance to disaster monitoring and decision making

Future plans to improve or develop further

Where it is being used or used in the past (geographical area)

Plans to scale up the product

Collaboration/support required (if any)

Contact person/organisation
